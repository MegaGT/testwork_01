﻿using UI;

namespace BaseTypes
{
    public class PlayerBattleSlot
    {
        public Player Player { get; private set; }
        public PlayerActionWidget ActionWidget { get; private set; }
        public int PlayerIndex { get; private set; }
        public int StepCounter { get; private set; }

        public PlayerBattleSlot(Player player, PlayerActionWidget actionWidget)
        {
            Player = player;
            ActionWidget = actionWidget;
        }

        public void SetPlayerIndex(in int index) => PlayerIndex = index;
        public void IncreaseStep() => ++StepCounter;
        public void ClearSteps() => StepCounter = 0;
    }
}