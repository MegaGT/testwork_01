using Logics.Damageable;
using Logics.Entities.Controllers;
using Logics.Skins;
using System;
using UnityEngine;

namespace BaseTypes
{
    public class Actor : MonoBehaviour, IDamageable
    {
        public event Action<float> TakingDamageEvent;

        [field: SerializeField] public Transform SelfTransform { get; private set; }
        [field: SerializeField] public SkinChanger SkinChanger { get; private set; }

        private Player _ownerPlayer;

        private HealthController _healthController;
        private ArmorController _armorController;
        private DamageController _damageController;

        public void Initialize(Player ownerPlayer)
        {
            _ownerPlayer = ownerPlayer;

            _armorController = _ownerPlayer.GetController<ArmorController>();
            _damageController = _ownerPlayer.GetController<DamageController>();

            _healthController = _ownerPlayer.GetController<HealthController>();
        }

        public float GetDamage()
        {
            if (_damageController != null)
            {
                return _damageController.GetTotalDamage();
            }

            return 0.0f;
        }

        public bool TryApplyDamage(float amount, out float pureDamage)
        {
            float totalDamage = amount;
            float resistance = 0.0f;

            if (_armorController != null)
            {
                resistance = _armorController.GetDamageResistance();
            }

            totalDamage -= (totalDamage * resistance);
            pureDamage = totalDamage;

            if (IsAlive())
            {
                _healthController.DecreaseHealth(totalDamage);
                TakingDamageEvent?.Invoke(totalDamage);

                return true;
            }

            pureDamage = 0.0f;
            return false;
        }

        public bool IsAlive()
        {
            if (_healthController != null)
            {
                return _healthController.IsAlive();
            }

            return false;
        }

        public MeshRenderer GetSkinRenderer()
        {
            if (SkinChanger != null && SkinChanger.SkinRenderer != null)
            {
                return SkinChanger.SkinRenderer;
            }

            return null;
        }
    }
}