﻿using Levels;
using Logics.Builders.Buffs;
using Logics.Entities;
using Logics.Entities.Controllers;
using Services.Events;
using Services.Locator;
using System.Collections.Generic;
using UI;
using UnityEngine;

namespace BaseTypes
{
    public class Player : MonoBehaviour, IInjectServices
    {
        [field: SerializeField] public string Nickname { get; private set; }
        [field: SerializeField] public ETeam Team { get; private set; }

        public Player EnemyTarget { get; private set; }

        [SerializeField] private ComponentConfig[] _components;
        [SerializeField] private Actor[] _controlledActors;

        [Header("Buffs param")]
        [SerializeField] private int _maxBuffCount;

        private List<EntityController> _controllers = new();

        private List<Buff> _buffs = new();
        private List<int> _buffsDuration = new();

        private LevelInstance _levelInstance;
        private ILevelEventsExec _levelEventsExec;
        private BuffStorage _buffStorage;
        private BuffDisplaying _buffDisplaying;

        public void Inject(IServiceLocator locator)
        {
            _levelInstance = locator.GetService<LevelInstance>();
            _levelEventsExec = locator.GetService<ILevelEventsExec>();
            _buffStorage = locator.GetService<BuffStorage>();
            _buffDisplaying = locator.GetService<BuffDisplaying>();
        }

        public void Start()
        {
            InitializeControllers();
            InitializeControlledActors();
        }

        private void Update()
        {
            for (int i = 0; i < _controllers.Count; ++i)
            {
                _controllers[i].Tick(Time.deltaTime);
            }
        }

        public void SetTarget(Player targetPlayer)
        {
            EnemyTarget = targetPlayer;
        }

        public void BuffDurationCheck()
        {
            for (int i = 0; i < _buffsDuration.Count; ++i)
            {
                --_buffsDuration[i];

                if (_buffsDuration[i] == 0)
                {
                    _buffs[i].Deactivate(this, EnemyTarget);
                    _buffsDuration.RemoveAt(i);
                    _buffs.RemoveAt(i);
                    --i;
                }
            }
        }

        public void ClearBuffs()
        {
            _buffsDuration.Clear();
            _buffs.Clear();
        }

        public void ApplyRandomBuff()
        {
            if (_buffs.Count >= _maxBuffCount) return;

            bool added = false;

            while (added == false)
            {
                int existCountBuff = 0;
                Buff randomBuff = _buffStorage.GetRandomBuff();

                for (int i = 0; i < _buffs.Count; ++i)
                {
                    if(randomBuff.GetType() == _buffs[i].GetType())
                    {
                        ++existCountBuff;
                    }
                }

                if (existCountBuff == 0) 
                {
                    _buffs.Add(randomBuff);
                    _buffsDuration.Add(randomBuff.Duration);

                    bool isLeftSideScreen = Team == _levelInstance.LevelConstants.LeftTeamPlayer;
                    _buffDisplaying.AddBuffWidget(randomBuff.Title, randomBuff.Duration, isLeftSideScreen);

                    randomBuff.Activate(this, EnemyTarget);
                    added = true;
                }

                if (existCountBuff > 0 && _buffStorage.CountBuffs() == 1)
                    return;

            }

        }

        public float GetTotalDamage()
        {
            float totalDamage = 0.0f;
            for (int i = 0; i < _controlledActors.Length; ++i)
            {
                totalDamage += _controlledActors[i].GetDamage();
            }

            return totalDamage;
        }

        public int GetCountAliveActors()
        {
            int counter = 0;
            for (int i = 0; i < _controlledActors.Length; ++i)
            {
                if (_controlledActors[i].IsAlive())
                    counter++;
            }

            return counter;
        }

        public bool TryApplyDamage(float amount, out float pureDamage)
        {
            float totalDamage = 0.0f;

            for (int i = 0; i < _controlledActors.Length; ++i)
            {
                if (_controlledActors[i].TryApplyDamage(amount, out float damage))
                {
                    totalDamage += damage;
                }
            }

            pureDamage = totalDamage;

            if (GetCountAliveActors() <= 0)
            {
                _levelEventsExec.OnLevelEnd(ELevelEndType.None);
            }

            return totalDamage > 0.0f;
        }

        public void ApplyVampiric(float damage)
        {
            var vampiricController = GetController<VampiricController>();
            var healthController = GetController<HealthController>();

            if(vampiricController != null) 
            {
                float vampiric = vampiricController.CalculateVampiric(damage);

                if(healthController != null)
                {
                    healthController.AddHealth(vampiric);
                }
            }
        }

        private void InitializeControllers()
        {
            for (int i = 0; i < _components.Length; ++i)
            {
                EntityController controller = _components[i].GetController(gameObject, Team);

                _controllers.Add(controller);
            }
        }

        private void InitializeControlledActors()
        {
            for (int i = 0; i < _controlledActors.Length; ++i)
            {
                _controlledActors[i].Initialize(this);
            }
        }

        public T GetController<T>() where T : EntityController
        {
            for (int i = 0; i < _controllers.Count; ++i)
            {
                if (_controllers[i] is T controller)
                {
                    return controller;
                }
            }

            return default;
        }

        private void OnDestroy()
        {
            for (int i = 0; i < _controllers.Count; ++i)
            {
                _controllers[i].Destruct();
            }
        }
    }
}