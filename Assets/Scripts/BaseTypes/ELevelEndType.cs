﻿namespace BaseTypes
{
    public enum ELevelEndType : byte
    {
        None = 0,
        Win,
        Lose
    }
}