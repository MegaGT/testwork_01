﻿using UnityEngine;

namespace Logics.Skins
{
    public class SkinChanger : MonoBehaviour
    {
        [field: SerializeField] public MeshRenderer SkinRenderer { get; private set; }

        [SerializeField] private Transform _skinSocket;

        public void ChangeSkin(Transform skinTransform)
        {
            for (int i = 0; i < _skinSocket.childCount; ++i)
            {
                Destroy(_skinSocket.GetChild(i).gameObject);
            }

            Transform skinInstance = Instantiate(skinTransform, _skinSocket);
            SkinRenderer = skinInstance.GetComponent<MeshRenderer>();
        }
    }
}