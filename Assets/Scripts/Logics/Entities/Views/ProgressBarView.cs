﻿using UI;
using UnityEngine;

namespace Logics.Entities.Views
{
    public class ProgressBarView : EntityView
    {
        private readonly ProgressBarService _progressBarService;

        private ProgressBarWidget _progressBarWidget;

        public ProgressBarView()
        {
            _progressBarService = GetLevel().ServiceLocator.GetService<ProgressBarService>();
        }

        public void InitWidget(bool isLeftScreen, string title, Color progressBarColor)
        {
            if (_progressBarService != null)
            {
                _progressBarWidget = _progressBarService.GetWidget(isLeftScreen);

                _progressBarWidget.SetTitleText(title);
                _progressBarWidget.SetProgressColor(progressBarColor);
            }
        }

        public void ChangeProgressColor(Color progressBarColor) => _progressBarWidget.SetProgressColor(progressBarColor);

        public void OnChangeProgress(float current, float max)
        {
            if (_progressBarWidget != null)
            {
                var currentText = Mathf.RoundToInt(current).ToString();
                var maxText = Mathf.RoundToInt(max).ToString();

                _progressBarWidget.SetAmountText(currentText + " / " + maxText);
                _progressBarWidget.UpdateProgress(current / max);
            }
        }

    }
}