﻿using BaseTypes;
using UnityEngine;

namespace Logics.Entities
{
    public class ComponentConfig : ScriptableObject
    {


        public virtual EntityController GetController(GameObject parent, ETeam ownerTeam)
            => new EntityController(parent, ownerTeam);
    }
}