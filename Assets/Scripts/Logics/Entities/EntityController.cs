﻿using BaseTypes;
using UnityEngine;

namespace Logics.Entities
{
    public class EntityController : LevelElement
    {
        public GameObject ParentObject { get; private set; }
        public ETeam Team { get; private set; }

        public EntityController(GameObject parentObject, ETeam team)
        {
            ParentObject = parentObject;
            Team = team;
        }

        public void SetParentGameObject(GameObject parent)
            => ParentObject = parent;

        public virtual void Tick(float deltaTick)
        {

        }

        public virtual void Destruct()
        {

        }
    }
}