﻿using System;
using UnityEngine;

namespace Logics.Entities.Models
{
    public class VampiricModel
    {
        public event Action<float, float> EventChangeVampiric;

        public float MaxVampiric { get; private set; }
        public float Vampiric { get; private set; }

        private float _boostVampiric;
        private float _decreaseVampiric;

        public VampiricModel(float vampiric, float maxVampiric)
        {
            Vampiric = vampiric;
            MaxVampiric = maxVampiric;
        }

        public void ChangeDecreaseVampiric(float amount)
        {
            _decreaseVampiric += amount;
            SendChangeVampiricEvent();
        }

        public void ApplyBoostVampiric(float boostVampiric)
        {
            _boostVampiric += boostVampiric;
            SendChangeVampiricEvent();
        }

        public void DecreaseBoostVampiric(float amount)
        {
            _boostVampiric -= amount;
            SendChangeVampiricEvent();
        }

        public float CalculateVampiric(float damage)
        {
            return damage * ((Vampiric + _boostVampiric - _decreaseVampiric) / 100.0f);
        }

        private void SendChangeVampiricEvent()
        {
            float vampiric = Mathf.Clamp((Vampiric + _boostVampiric) - _decreaseVampiric, 0.0f, MaxVampiric);
            EventChangeVampiric?.Invoke(vampiric, MaxVampiric);
        }
    }
}