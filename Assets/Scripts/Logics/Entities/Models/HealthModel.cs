﻿using System;
using UnityEngine;

namespace Logics.Entities.Models
{
    public class HealthModel
    {
        public event Action<float, float> EventChangeHealth;

        public float MaxHealth { get; private set; }
        public float CurrentHealth { get; private set; }

        public HealthModel(float current, float max)
        {
            MaxHealth = max;
            CurrentHealth = current;
        }

        public void ChangeHealth(float totalHealth)
        {
            CurrentHealth = Mathf.Clamp(totalHealth, 0.0f, MaxHealth);
            EventChangeHealth?.Invoke(CurrentHealth, MaxHealth);
        }

        public bool IsAlive()
        {
            return CurrentHealth > 0.0f;
        }
    }
}