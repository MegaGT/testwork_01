﻿using System;
using UnityEngine;

namespace Logics.Entities.Models
{
    public class DamageModel
    {
        public event Action<float> DamageChanged;

        public float Damage { get; private set; }

        private float _boostDamage;

        public DamageModel(float damage)
        {
            Damage = damage;
        }

        public float GetTotalDamage() => Damage + _boostDamage;
        public void ApplyBoostDamage(float boostDamage) => _boostDamage += boostDamage;
        public void DecreaseBoostDamage(float amount) => _boostDamage -= amount;

        public void ChangeDamage(float damage)
        {
            Damage = Mathf.Max(damage, 0.0f);
            DamageChanged?.Invoke(Damage);
        }
    }
}