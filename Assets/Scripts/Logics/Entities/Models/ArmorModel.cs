﻿using System;
using UnityEngine;

namespace Logics.Entities.Models
{
    public class ArmorModel
    {
        public event Action<float, float> EventChangeArmor;

        public float MaxArmor { get; private set; }
        public float Armor { get; private set; }

        private float _boostArmor;
        private float _decreaseArmor;

        public ArmorModel(float armor, float maxArmor)
        {
            Armor = armor;
            MaxArmor = maxArmor;
        }

        public void ChangeDecreaseArmor(float amount)
        {
            _decreaseArmor += amount;
            SendArmorChangeEvent();
        }

        public void ApplyBoostArmor(float boostArmor)
        {
            _boostArmor += boostArmor;

            SendArmorChangeEvent();
        }

        public void DecreaseBoostArmor(float amount)
        {
            _boostArmor -= amount;

            SendArmorChangeEvent();
        }

        public float GetDamageResistance()
        {
            float totalArmor = Mathf.Clamp(CalculateCurrentArmor(), 0.0f, MaxArmor);

            return totalArmor / 100.0f;
        }

        private float CalculateCurrentArmor() => (Armor + _boostArmor) - _decreaseArmor;

        private void SendArmorChangeEvent()
        {
            float totalArmor = Mathf.Clamp(CalculateCurrentArmor(), 0.0f, MaxArmor);
            EventChangeArmor?.Invoke(totalArmor, MaxArmor);
        }
    }
}