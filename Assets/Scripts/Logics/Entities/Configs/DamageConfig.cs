﻿using BaseTypes;
using Logics.Entities.Controllers;
using Logics.Entities.Models;
using UnityEngine;

namespace Logics.Entities.Configs
{
    [CreateAssetMenu(menuName = "Game/Components/Damage", fileName = "DamageConfig")]
    public class DamageConfig : ComponentConfig
    {
        [field: SerializeField][Min(1)] public float Damage;

        public override EntityController GetController(GameObject parent, ETeam ownerTeam)
        {
            return new DamageController(new DamageModel(Damage), parent, ownerTeam);
        }
    }
}