﻿using BaseTypes;
using Logics.Entities.Controllers;
using Logics.Entities.Models;
using Logics.Entities.Views;
using UnityEngine;

namespace Logics.Entities.Configs
{
    [CreateAssetMenu(menuName = "Game/Components/Vampiric", fileName = "VampiricConfig")]
    public class VampiricConfig : ComponentConfig
    {
        [SerializeField][Range(0, 100)] private float _maxVampiric;
        [SerializeField][Range(0, 100)] private float _vampiric;

        public override EntityController GetController(GameObject parent, ETeam ownerTeam)
        {
            return new VampiricController(new VampiricModel(_vampiric, _maxVampiric), new ProgressBarView(),
                                                                parent, ownerTeam);
        }
    }
}