using BaseTypes;
using Logics.Entities.Controllers;
using Logics.Entities.Models;
using Logics.Entities.Views;
using UnityEngine;

namespace Logics.Entities.Configs
{
    [CreateAssetMenu(menuName = "Game/Components/Health", fileName = "Health")]
    public class HealthConfig : ComponentConfig
    {
        [SerializeField][Range(0, 100)] private float _maxHealth;
        [SerializeField][Range(0, 100)] private float _currentHealth;

        public override EntityController GetController(GameObject parent, ETeam ownerTeam)
        {
            return new HealthController(new HealthModel(_currentHealth, _maxHealth), new ProgressBarView(),
                                                            parent, ownerTeam);
        }
    }
}