﻿using BaseTypes;
using Logics.Entities.Controllers;
using Logics.Entities.Models;
using Logics.Entities.Views;
using UnityEngine;

namespace Logics.Entities.Configs
{
    [CreateAssetMenu(menuName = "Game/Components/Armor", fileName = "ArmorConfig")]
    public class ArmorConfig : ComponentConfig
    {
        [SerializeField][Range(0, 100)] private float _maxArmor;
        [SerializeField][Range(0, 100)] private float _armor;

        public override EntityController GetController(GameObject parent, ETeam ownerTeam)
        {
            return new ArmorController(new ArmorModel(_armor, _maxArmor), new ProgressBarView(), parent, ownerTeam);
        }
    }
}