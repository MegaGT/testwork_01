﻿using Levels;
using UnityEngine;

namespace Logics.Entities
{
    public class LevelElement
    {
        private LevelInstance _levelInstance;

        public LevelInstance GetLevel()
        {
            if (_levelInstance == null)
            {
                _levelInstance = Object.FindObjectOfType<LevelInstance>();
                return _levelInstance;
            }
            else
            {
                return _levelInstance;
            }
        }
    }
}