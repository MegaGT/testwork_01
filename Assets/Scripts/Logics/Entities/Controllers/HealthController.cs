﻿using BaseTypes;
using Levels;
using Logics.Entities.Models;
using Logics.Entities.Views;
using System;
using UnityEngine;

namespace Logics.Entities.Controllers
{
    public class HealthController : EntityController
    {
        public event Action EventDeath;

        private readonly HealthModel _model;
        private readonly ProgressBarView _view;

        public HealthController(HealthModel model, ProgressBarView view, GameObject parentObject, ETeam ownerTeam)
            : base(parentObject, ownerTeam)
        {
            _model = model;
            _view = view;


            bool isLeftScreenSide = ownerTeam == GetLevel().LevelConstants.LeftTeamPlayer;
            _view.InitWidget(isLeftScreenSide, Constants.HEALTH_TITLE, GetLevel().LevelConstants.HealthProgressColor);

            _model.EventChangeHealth += OnEventChangeHealth;

            OnEventChangeHealth(_model.CurrentHealth, _model.MaxHealth);
        }

        private void OnEventChangeHealth(float current, float max)
        {
            _view.OnChangeProgress(current, max);

            if (_model.IsAlive() == false)
            {
                EventDeath?.Invoke();
                return;
            }
        }

        public void AddHealth(float amount)
        {
            _model.ChangeHealth(_model.CurrentHealth + amount);
        }

        public void DecreaseHealth(float amount)
        {
            _model.ChangeHealth(_model.CurrentHealth - amount);
        }

        public bool IsAlive() => _model.IsAlive();

        public override void Destruct()
        {
            _model.EventChangeHealth -= OnEventChangeHealth;
        }
    }
}