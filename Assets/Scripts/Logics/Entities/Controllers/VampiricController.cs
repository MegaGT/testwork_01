﻿using BaseTypes;
using Levels;
using Logics.Entities.Models;
using Logics.Entities.Views;
using UnityEngine;

namespace Logics.Entities.Controllers
{
    public class VampiricController : EntityController
    {
        private readonly VampiricModel _model;
        private readonly ProgressBarView _view;

        public VampiricController(VampiricModel model, ProgressBarView view, GameObject parentObject, ETeam team) :
            base(parentObject, team)
        {
            _model = model;
            _view = view;

            bool isLeftScreenSide = team == GetLevel().LevelConstants.LeftTeamPlayer;
            _view.InitWidget(isLeftScreenSide, Constants.VAMPIRIC_TITLE, GetLevel().LevelConstants.VampiricProgressColor);

            _model.EventChangeVampiric += OnEventChangeVampiric;

            OnEventChangeVampiric(_model.Vampiric, _model.MaxVampiric);
        }

        public void ApplyBoostVampiric(float boostVampiric) => _model.ApplyBoostVampiric(boostVampiric);
        public void DecreaseBoostVampiric(float amount) => _model.DecreaseBoostVampiric(amount);
        public float CalculateVampiric(float damage) => _model.CalculateVampiric(damage);
        public void ChangeDecreaseVampiric(float amount) => _model.ChangeDecreaseVampiric(amount);

        private void OnEventChangeVampiric(float current, float max)
        {
            _view.OnChangeProgress(current, max);
        }
    }
}