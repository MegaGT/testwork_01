﻿using BaseTypes;
using Logics.Entities.Models;
using UnityEngine;

namespace Logics.Entities.Controllers
{
    public class DamageController : EntityController
    {
        private readonly DamageModel _model;

        public DamageController(DamageModel model, GameObject parentObject, ETeam team) :
            base(parentObject, team)
        {
            _model = model;
        }

        public float GetTotalDamage() => _model.GetTotalDamage();
        public void ApplyBoostDamage(float boostDamage) => _model.ApplyBoostDamage(boostDamage);
        public void DecreaseBoostDamage(float amount) => _model.DecreaseBoostDamage(amount);
    }
}