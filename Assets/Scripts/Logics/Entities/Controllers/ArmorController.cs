﻿using BaseTypes;
using Levels;
using Logics.Entities.Models;
using Logics.Entities.Views;
using UnityEngine;

namespace Logics.Entities.Controllers
{
    public class ArmorController : EntityController
    {
        private readonly ArmorModel _model;
        private readonly ProgressBarView _view;

        public ArmorController(ArmorModel model, ProgressBarView view, GameObject parentObject, ETeam team) :
            base(parentObject, team)
        {
            _model = model;
            _view = view;

            bool isLeftScreenSide = team == GetLevel().LevelConstants.LeftTeamPlayer;
            _view.InitWidget(isLeftScreenSide, Constants.ARMOR_TITLE, GetLevel().LevelConstants.ArmorProgressColor);

            _model.EventChangeArmor += OnEventChangeArmor;

            OnEventChangeArmor(_model.Armor, _model.MaxArmor);
        }

        public float GetDamageResistance() => _model.GetDamageResistance();
        public void ApplyBoostArmor(float boostArmor) => _model.ApplyBoostArmor(boostArmor);
        public void DecreaseBoostArmor(float amount) => _model.DecreaseBoostArmor(amount);
        public void ChangeDecreaseArmor(float amount) => _model.ChangeDecreaseArmor(amount);

        private void OnEventChangeArmor(float current, float max)
        {
            _view.OnChangeProgress(current, max);
        }

        public override void Destruct()
        {
            _model.EventChangeArmor -= OnEventChangeArmor;
        }
    }
}