﻿namespace Logics.Damageable
{
    public interface IDamageable
    {
        bool IsAlive();
        bool TryApplyDamage(float amount, out float pureDamage);
    }
}