﻿using BaseTypes;
using System.Collections;
using UnityEngine;

namespace Logics.Damageable
{
    public class DamageableVFX : MonoBehaviour
    {
        [SerializeField][Min(0.5f)] private float _duration;
        [SerializeField] private Color _damageColor;

        [Header("References")]
        [SerializeField] private Actor _actor;

        private MeshRenderer _skinRenderer;
        private Color _originalColor;

        private void OnEnable()
        {
            if (_actor != null)
            {
                _skinRenderer = _actor.GetSkinRenderer();

                if (_skinRenderer != null)
                    _originalColor = _skinRenderer.material.color;

                _actor.TakingDamageEvent += OnTakingDamageEvent;
            }
        }

        private void OnDisable()
        {
            if (_actor != null)
                _actor.TakingDamageEvent -= OnTakingDamageEvent;
        }

        public void OnTakingDamageEvent(float amount)
        {
            if (_skinRenderer != null && amount > 0.0f)
            {
                StartCoroutine(DamageColorLerp());
            }
        }

        private IEnumerator DamageColorLerp()
        {
            float totalDuration = _duration / 2;
            float timer = 0.0f;

            while (timer < totalDuration)
            {
                ApplyDamageColorLerp(timer / totalDuration);
                timer += Time.deltaTime;

                yield return null;
            }

            while (timer > 0.0f)
            {
                ApplyDamageColorLerp(timer / totalDuration);
                timer -= Time.deltaTime;

                yield return null;
            }


        }

        private void ApplyDamageColorLerp(float lerpDelta)
        {
            Color endColor = Color.Lerp(_originalColor, _damageColor, lerpDelta);

            if (_skinRenderer != null)
            {
                _skinRenderer.material.color = endColor;
            }
        }
    }
}