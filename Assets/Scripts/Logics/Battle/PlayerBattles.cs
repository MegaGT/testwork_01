﻿using BaseTypes;
using Levels;
using Services.Events;
using Services.Locator;
using System.Collections.Generic;
using UI;
using UnityEngine;

namespace Logics.Battle
{
    public class PlayerBattles : MonoBehaviour, IInjectServices
    {
        [SerializeField] private Player[] _players;
        [SerializeField] private ETeam _teamIsFirstStep;

        private int _roundCounter;
        private bool _isRunning;

        private List<PlayerBattleSlot> _playersSlot = new();
        private PlayerBattleSlot _currentPlayer;

        private ILevelEvents _levelEvents;
        private ILevelEventsExec _levelEventsExec;
        private LevelInstance _levelInstance;

        public void Inject(IServiceLocator locator)
        {
            _levelEvents = locator.GetService<ILevelEvents>();
            _levelEventsExec = locator.GetService<ILevelEventsExec>();
            _levelInstance = locator.GetService<LevelInstance>();

            _levelEvents.LevelEnd += OnLevelEnd;

            var playerActionUI = locator.GetService<PlayerActionUI>();
            var levelInstance = locator.GetService<LevelInstance>();

            for (int i = 0; i < _players.Length; ++i)
            {
                bool isLeftSideScreen = _players[i].Team == levelInstance.LevelConstants.LeftTeamPlayer;

                PlayerActionWidget actionWidget = playerActionUI.GetWidget(isLeftSideScreen);

                actionWidget.OnAttackButtonEvent += OnAttackButtonEvent;
                actionWidget.OnApplyBuffButtonEvent += OnApplyBuffButtonEvent;
                actionWidget.DisableButtons();

                var playerSlot = new PlayerBattleSlot(_players[i], actionWidget);

                playerSlot.SetPlayerIndex(i);

                _playersSlot.Add(playerSlot);
            }
        }

        private void OnLevelEnd(ELevelEndType endType)
        {
            StopBattler();
            _levelInstance.RestartLevel();
        }

        private void Start()
        {
            StartBattle();
        }

        public void StartBattle()
        {
            for (int i = 0; i < _playersSlot.Count; ++i)
            {
                for (int j = 0; j < _playersSlot.Count; ++j)
                {
                    if (_playersSlot[i].Player.Team != _playersSlot[j].Player.Team)
                    {
                        _playersSlot[i].Player.SetTarget(_playersSlot[j].Player);
                    }
                }
            }

            for (int i = 0; i < _playersSlot.Count; ++i)
            {
                if (_playersSlot[i].Player.Team == _teamIsFirstStep)
                {
                    _currentPlayer = _playersSlot[i];
                    _currentPlayer.ActionWidget.EnableButtons();
                    break;
                }
            }

            _isRunning = true;
        }

        private void OnAttackButtonEvent()
        {
            if (_currentPlayer != null && _isRunning)
            {
                float totalDamage = _currentPlayer.Player.GetTotalDamage();
                float totalApplayingDamage = 0.0f;

                for (int i = 0; i < _playersSlot.Count; ++i)
                {
                    if (_playersSlot[i].Player.Team != _currentPlayer.Player.Team)
                    {
                        if (_playersSlot[i].Player.TryApplyDamage(totalDamage, out float damage))
                        {
                            totalApplayingDamage += damage;
                        }
                    }
                }

                if (_isRunning)
                {
                    _currentPlayer.Player.ApplyVampiric(totalApplayingDamage);
                    _currentPlayer.Player.BuffDurationCheck();
                    _currentPlayer.IncreaseStep();
                    SwitchPlayersStep();
                }
            }
        }

        private void OnApplyBuffButtonEvent()
        {
            if(_currentPlayer != null && _isRunning)
            {
                _currentPlayer.Player.ApplyRandomBuff();
            }
        }

        private void SwitchPlayersStep()
        {
            _currentPlayer.ActionWidget.DisableButtons();

            if (CheckRound())
            {
                ++_roundCounter;
                _levelEventsExec.OnNextRound(_roundCounter);
                ClearSteps();
            }

            int nextPlayerIndex = _currentPlayer.PlayerIndex + 1;
            _currentPlayer = null;

            if (nextPlayerIndex >= _playersSlot.Count)
            {
                nextPlayerIndex = 0;
            }

            _currentPlayer = _playersSlot[nextPlayerIndex];
            _currentPlayer.ActionWidget.EnableButtons();
        }

        private bool CheckRound()
        {
            int countSteps = 0;

            for (int i = 0; i < _playersSlot.Count; ++i)
            {
                countSteps += _playersSlot[i].StepCounter;
            }

            if (countSteps >= _playersSlot.Count)
            {
                return true;
            }

            return false;
        }

        private void ClearSteps()
        {
            for (int i = 0; i < _playersSlot.Count; ++i)
            {
                _playersSlot[i].ClearSteps();
            }
        }

        private void StopBattler()
        {
            _currentPlayer?.ActionWidget.DisableButtons();

            for (int i = 0; i < _playersSlot.Count; ++i)
            {
                _playersSlot[i].Player.ClearBuffs();
            }

            _isRunning = false;
        }

        private void OnDestroy()
        {
            for (int i = 0; i < _playersSlot.Count; ++i)
            {
                _playersSlot[i].ActionWidget.OnAttackButtonEvent -= OnAttackButtonEvent;
                _playersSlot[i].ActionWidget.OnApplyBuffButtonEvent -= OnApplyBuffButtonEvent;
            }

            _levelEvents.LevelEnd -= OnLevelEnd;
        }
    }
}