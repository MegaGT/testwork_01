﻿using BaseTypes;
using Logics.Entities.Controllers;
using System.Collections;
using UnityEngine;

namespace Logics.Builders.Buffs
{
    public class ArmorSelfBuff : Buff
    {
        [field: SerializeField] public float SelfArmor { get; private set; }

        public override void Activate(Player ownerPlayer, Player enemyTarget)
        {
            var armorController = ownerPlayer.GetController<ArmorController>();

            if(armorController != null) 
            {
                armorController.ApplyBoostArmor(SelfArmor);
            }
        }

        public override void Deactivate(Player ownerPlayer, Player enemyTarget)
        {
            var armorController = ownerPlayer.GetController<ArmorController>();

            if (armorController != null)
            {
                armorController.DecreaseBoostArmor(SelfArmor);
            }
        }
    }
}