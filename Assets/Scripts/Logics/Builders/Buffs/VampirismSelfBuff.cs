﻿using BaseTypes;
using Logics.Entities.Controllers;
using System.Collections;
using UnityEngine;

namespace Logics.Builders.Buffs
{
    public class VampirismSelfBuff : Buff
    {
        [field: SerializeField] public float Vampirism { get; private set; }
        [field: SerializeField] public float DecreaseSelfArmor { get; private set; }

        public override void Activate(Player ownerPlayer, Player enemyTarget)
        {
            var vampirism = ownerPlayer.GetController<VampiricController>();
            var armor = ownerPlayer.GetController<ArmorController>();

            if(vampirism != null)
            {
                vampirism.ApplyBoostVampiric(Vampirism);
            }

            if(armor != null)
            {
                armor.ChangeDecreaseArmor(DecreaseSelfArmor);
            }
        }
        public override void Deactivate(Player ownerPlayer, Player enemyTarget) 
        {
            var vampirism = ownerPlayer.GetController<VampiricController>();
            var armor = ownerPlayer.GetController<ArmorController>();

            if (vampirism != null)
            {
                vampirism.DecreaseBoostVampiric(Vampirism);
            }

            if (armor != null)
            {
                armor.ChangeDecreaseArmor(DecreaseSelfArmor * -1.0f);
            }
        }
    }
}