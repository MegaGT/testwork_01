﻿using BaseTypes;
using Logics.Entities.Configs;
using Logics.Entities.Controllers;
using System.Collections;
using UnityEditor.Timeline;
using UnityEngine;

namespace Logics.Builders.Buffs
{
    public class DoubleDamageBuff : Buff
    {
        [field: SerializeField] public int MultiplyDamage;

        public override void Activate(Player ownerPlayer, Player enemyTarget)
        {
            var damageController = ownerPlayer.GetController<DamageController>();

            if(damageController != null ) 
            {
                float damage = damageController.GetTotalDamage();

                float boostDamage = damage * MultiplyDamage;
                boostDamage -= damage;

                damageController.ApplyBoostDamage(boostDamage);
            }
        }

        public override void Deactivate(Player ownerPlayer, Player enemyTarget)
        {
            var damageController = ownerPlayer.GetController<DamageController>();

            if (damageController != null)
            {
                float damage = damageController.GetTotalDamage();

                float originDamage = damage / MultiplyDamage;
                damage -= originDamage;

                damageController.DecreaseBoostDamage(damage);
            }
        }

        private float CalculateDamage(DamageController damageController)
        {
            float damage = damageController.GetTotalDamage();

            float boostDamage = damage * MultiplyDamage;
            boostDamage -= damage;

            return boostDamage;
        }
    }
}