﻿using System.Collections;
using UnityEngine;

namespace Logics.Builders.Buffs
{
    public class BuffStorage : MonoBehaviour
    {
        [SerializeField] private Buff[] _buffs;

        public Buff GetRandomBuff()
        {
            int randomIndex = Random.Range(0, _buffs.Length);
            return _buffs[randomIndex];
        }

        public int CountBuffs() => _buffs.Length;
    }
}