﻿using BaseTypes;
using UnityEngine;

namespace Logics.Builders.Buffs
{
    public class Buff : MonoBehaviour
    {
        [field: SerializeField] public string Title { get; private set; }
        [field: SerializeField] public int Duration { get; private set; }

        public virtual void Activate(Player ownerPlayer, Player enemyTarget)
        {

        }

        public virtual void Deactivate(Player ownerPlayer, Player enemyTarget) 
        {
            
        }
    }
}