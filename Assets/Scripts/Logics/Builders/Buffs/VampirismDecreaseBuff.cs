﻿using BaseTypes;
using Logics.Entities.Controllers;
using UnityEngine;

namespace Logics.Builders.Buffs
{
    public class VampirismDecreaseBuff : Buff
    {
        [field: SerializeField] public float DecreaseEnemyVampirism { get; private set; }

        public override void Activate(Player ownerPlayer, Player enemyTarget)
        {
            ChangeEnemyVampirism(enemyTarget, DecreaseEnemyVampirism);
        }

        public override void Deactivate(Player ownerPlayer, Player enemyTarget)
        {
            ChangeEnemyVampirism(enemyTarget, DecreaseEnemyVampirism * -1.0f);
        }

        private void ChangeEnemyVampirism(Player enemy, float vampiric)
        {
            if (enemy != null)
            {
                var enemyVampiricController = enemy.GetController<VampiricController>();

                if (enemyVampiricController != null)
                {
                    enemyVampiricController.ChangeDecreaseVampiric(vampiric);
                }
            }
        }
    }
}