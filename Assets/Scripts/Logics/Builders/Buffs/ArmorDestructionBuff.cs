﻿using BaseTypes;
using Logics.Entities.Controllers;
using System.Collections;
using UnityEngine;

namespace Logics.Builders.Buffs
{
    public class ArmorDestructionBuff : Buff
    {
        [field: SerializeField] public float DecreaseEnemyArmor { get; private set; }

        public override void Activate(Player ownerPlayer, Player enemyTarget)
        {
            if (enemyTarget != null)
            {
                var enemyArmorController = enemyTarget.GetController<ArmorController>();

                if (enemyArmorController != null) 
                {
                    enemyArmorController.ChangeDecreaseArmor(DecreaseEnemyArmor);
                }
            }
        }

        public override void Deactivate(Player ownerPlayer, Player enemyTarget)
        {
            if(enemyTarget != null) 
            {
                var enemyArmorController = enemyTarget.GetController<ArmorController>();
                
                if(enemyArmorController != null)
                {
                    enemyArmorController.ChangeDecreaseArmor(-DecreaseEnemyArmor);
                }
            }
        }
    }
}