﻿using BaseTypes;

namespace Logics.Builders
{
    public class ActorBuilder
    {
        protected Actor ActorInstance;

        public virtual void Make()
        {

        }

        public virtual Actor GetResult() => ActorInstance;
    }
}