using BaseTypes;
using System;

namespace Services.Events
{
    public class LevelEvents : ILevelEvents, ILevelEventsExec
    {
        public event Action LevelInitialized;
        public event Action LevelReady;
        public event Action<ELevelEndType> LevelEnd;

        public event Action<int> NextRound;

        public void OnLevelInitialized() => LevelInitialized?.Invoke();
        public void OnLevelReady() => LevelReady?.Invoke();
        public void OnLevelEnd(ELevelEndType endType) => LevelEnd?.Invoke(endType);


        public void OnNextRound(int roundIndex) => NextRound?.Invoke(roundIndex);
    }
}