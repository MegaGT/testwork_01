﻿using BaseTypes;

namespace Services.Events
{
    public interface ILevelEventsExec
    {
        void OnLevelEnd(ELevelEndType endType);
        void OnLevelInitialized();
        void OnLevelReady();
        void OnNextRound(int roundIndex);
    }
}