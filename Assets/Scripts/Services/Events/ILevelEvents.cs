﻿using BaseTypes;
using System;

namespace Services.Events
{
    public interface ILevelEvents
    {
        event Action LevelInitialized;
        event Action LevelReady;
        event Action<int> NextRound;
        event Action<ELevelEndType> LevelEnd;
    }
}