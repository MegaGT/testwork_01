﻿using BaseTypes;
using UnityEngine;

namespace Levels
{
    [CreateAssetMenu(menuName = "Game/Constants/LevelConstants", fileName = "LevelConstants")]
    public class LevelConstants : ScriptableObject
    {
        [field: SerializeField] public Color HealthProgressColor;
        [field: SerializeField] public Color ArmorProgressColor;
        [field: SerializeField] public Color VampiricProgressColor;
        [Space]
        [field: SerializeField] public ETeam LeftTeamPlayer;
        [field: SerializeField] public ETeam RightTeamPlayer;
    }
}