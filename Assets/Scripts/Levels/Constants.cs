﻿namespace Levels
{
    public static class Constants
    {
        public static readonly int GAME_FRAME_RATE = 59;

        public static readonly string HEALTH_TITLE = "Health";
        public static readonly string ARMOR_TITLE = "Armor";
        public static readonly string VAMPIRIC_TITLE = "Vampiric";

        public static readonly string ROUND_COUNTER_PREFIX = "Round: ";
    }
}