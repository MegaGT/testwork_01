﻿using Logics.Builders.Buffs;
using Services.Events;
using Services.Locator;
using UI;
using UnityEngine;

namespace Levels
{
    public class LevelServices : MonoBehaviour
    {
        [SerializeField] private BuffStorage _buffStorage;
        [SerializeField] private ProgressBarService _progressBarService;
        [SerializeField] private PlayerActionUI _playerActionUI;
        [SerializeField] private BuffDisplaying _buffDisplaying;

        private ServiceLocator _serviceLocator;

        private LevelEvents _levelEvents;

        public void Init(LevelInstance levelInstance)
        {
            _serviceLocator = new ServiceLocator();

            _serviceLocator.RegisterService<LevelInstance>(levelInstance);

            RegistrationOfServices();
            InjectServicesInSceneObjects();
        }

        private void RegistrationOfServices()
        {
            _levelEvents = new LevelEvents();

            _serviceLocator
                .RegisterService<ILevelEvents>(_levelEvents)
                .RegisterService<ILevelEventsExec>(_levelEvents)
                .RegisterService<ProgressBarService>(_progressBarService)
                .RegisterService<PlayerActionUI>(_playerActionUI)
                .RegisterService<BuffStorage>(_buffStorage)
                .RegisterService<BuffDisplaying>(_buffDisplaying);
        }

        public IServiceLocator GetServiceLocator() => _serviceLocator;

        private void InjectServicesInSceneObjects()
        {
            foreach (var dependency in FindObjectsOfType<MonoBehaviour>())
            {
                if (dependency is IInjectServices patient)
                {
                    patient.Inject(_serviceLocator);
                }
            }
        }
    }
}