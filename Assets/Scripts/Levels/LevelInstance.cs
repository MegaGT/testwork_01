using Services.Locator;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Levels
{
    public class LevelInstance : MonoBehaviour
    {
        public IServiceLocator ServiceLocator { get; private set; }

        [field: SerializeField] public LevelConstants LevelConstants { get; private set; }

        [SerializeField] private LevelServices _levelServices;

        private void Awake()
        {
            SetBaseAppSettings();
            InitServices();
        }
        public void RestartLevel()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        private void SetBaseAppSettings()
        {
            Application.targetFrameRate = Constants.GAME_FRAME_RATE;
        }

        private void InitServices()
        {
            _levelServices?.Init(this);
            ServiceLocator = _levelServices.GetServiceLocator();
        }

    }
}