﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class PlayerActionWidget : MonoBehaviour
    {
        public event Action OnAttackButtonEvent;
        public event Action OnApplyBuffButtonEvent;

        [SerializeField] private Button _attackButton;
        [SerializeField] private Button _applyBuffButton;

        public void OnAttackButton()
        {
            OnAttackButtonEvent?.Invoke();
        }

        public void OnApplyBuffButton()
        {
            OnApplyBuffButtonEvent?.Invoke();
        }

        public void EnableButtons()
        {
            _attackButton.interactable = true;
            _applyBuffButton.interactable = true;
        }

        public void DisableButtons()
        {
            _attackButton.interactable = false;
            _applyBuffButton.interactable = false;
        }
    }
}