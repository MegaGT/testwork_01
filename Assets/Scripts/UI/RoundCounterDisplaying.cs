﻿using Levels;
using Services.Events;
using Services.Locator;
using TMPro;
using UnityEngine;

namespace UI
{
    public class RoundCounterDisplaying : MonoBehaviour, IInjectServices
    {
        [SerializeField] private TMP_Text _roundCounterText;

        private ILevelEvents _levelEvents;

        public void Inject(IServiceLocator locator)
        {
            _levelEvents = locator.GetService<ILevelEvents>();

            _levelEvents.NextRound += OnNextRound;
        }

        private void OnNextRound(int roundIndex)
        {
            _roundCounterText.text = Constants.ROUND_COUNTER_PREFIX + (roundIndex + 1).ToString();
        }

        private void OnDestroy()
        {
            _levelEvents.NextRound -= OnNextRound;
        }
    }
}