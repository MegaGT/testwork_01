﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class ProgressBarWidget : MonoBehaviour
    {
        [SerializeField] private Image _progressImage;
        [SerializeField] private TMP_Text _titleText;
        [SerializeField] private TMP_Text _amountText;

        public void SetTitleText(string title) => _titleText.text = title;
        public void SetAmountText(string amountText) => _amountText.text = amountText;
        public void SetProgressColor(Color color) => _progressImage.color = color;
        public void UpdateProgress(float progress) => _progressImage.fillAmount = progress;
    }
}