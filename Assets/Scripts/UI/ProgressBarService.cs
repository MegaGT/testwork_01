﻿using UnityEngine;

namespace UI
{
    public class ProgressBarService : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private RectTransform _leftContainer;
        [SerializeField] private RectTransform _rightContainer;

        [Header("Prefabs")]
        [SerializeField] private ProgressBarWidget _widgetPrefab;

        public ProgressBarWidget GetWidget(bool isLeftScreen)
        {
            var parent = isLeftScreen ? _leftContainer : _rightContainer;

            return Instantiate(_widgetPrefab, parent);
        }
    }
}