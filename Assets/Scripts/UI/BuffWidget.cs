﻿using TMPro;
using UnityEngine;

namespace UI
{
    public class BuffWidget : MonoBehaviour
    {
        [SerializeField] private GameObject _parentObject;
        [SerializeField] private TMP_Text _widgetText;

        public void SetText(string text)
            => _widgetText.text = text;

        public void Show() => _parentObject.SetActive(true);
        public void Hide() => _parentObject.SetActive(false);
    }
}