﻿using UnityEngine;

namespace UI
{
    public class PlayerActionUI : MonoBehaviour
    {
        [SerializeField] private RectTransform _leftScreenSide;
        [SerializeField] private RectTransform _rightScreenSide;

        [Header("Prefabs")]
        [SerializeField] private PlayerActionWidget _actionWidgetPrefab;

        public PlayerActionWidget GetWidget(bool isLeftScreenSide)
        {
            RectTransform side = isLeftScreenSide ? _leftScreenSide : _rightScreenSide;

            return Instantiate(_actionWidgetPrefab, side);
        }
    }
}