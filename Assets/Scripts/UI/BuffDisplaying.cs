﻿using System.Collections;
using UnityEngine;

namespace UI
{
    public class BuffDisplaying : MonoBehaviour
    {
        [SerializeField] private RectTransform _leftSideContainer;
        [SerializeField] private RectTransform _rightSideContainer;
        [SerializeField] private BuffWidget _widgetPrefab;

        public void AddBuffWidget(string title, int duration, bool isLeftSideScreen)
        {
            RectTransform side = isLeftSideScreen ? _leftSideContainer : _rightSideContainer;

            BuffWidget widget = Instantiate(_widgetPrefab, side);

            widget.SetText(title + ": " + duration.ToString());
            widget.Show();
        }
    }
}